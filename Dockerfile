# Use the official Python 3.11 image from the Docker Hub
FROM python:3.11-slim

# Install necessary system packages, including curl
RUN apt-get update && apt-get install -y \
    build-essential \
    python3-dev \
 && rm -rf /var/lib/apt/lists/*


# Set the working directory inside the container
WORKDIR /app

# Copy the requirements file and install dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy the entire current directory contents into the container at /app
COPY . .

# Run the Python script using asyncio
CMD ["python", "-u", "main.py"]

